#!/usr/bin/env bash

export BUILDPACK_STDLIB_URL="https://lang-common.s3.amazonaws.com/buildpack-stdlib/v7/stdlib.sh"

detect_mill() {
  local ctxDir=$1
  if _has_scFile $ctxDir; then
    return 0
  else
    return 1
  fi
}

_has_scFile() {
  local ctxDir=$1
  test -n "$(find $ctxDir -maxdepth 1 -name 'build.sc' -print -quit)"
}


count_files() {
  local location=$1
  local pattern=$2

  if [ -d ${location} ]; then
    find ${location} -name ${pattern} | wc -l | sed 's/ //g'
  else
    echo "0"
  fi
}

is_app_dir() {
  test "$1" != "/app"
}

# sed -l basically makes sed replace and buffer through stdin to stdout
# so you get updates while the command runs and dont wait for the end
# e.g. sbt stage | indent
output() {
  local logfile="$1"
  local c='s/^/       /'

  case $(uname) in
      Darwin) tee -a "$logfile" | sed -l "$c";; # mac/bsd sed: -l buffers on line boundaries
      *)      tee -a "$logfile" | sed -u "$c";; # unix/gnu sed: -u unbuffered (arbitrary) chunks of data
  esac
}


install_mill() {
  local millBinDir=${1}

  status "Looking for Mill at '${millBinDir}/mill'..."

  if [[ -n "${millBinDir}/mill" ]]; then
    status "Mill not detected.  Installing..."
    mkdir -p ${millBinDir}
    curl -L https://github.com/lihaoyi/mill/releases/download/0.10.0/0.10.0 > ${millBinDir}/mill && chmod +x ${millBinDir}/mill;
  else
    status "Mill detected."
  fi

  status "Checking Mill cache..."
  mkdir -p .mill
  ls .mill

  ${millBinDir}/mill -i -h .mill version
  export PATH="${millBinDir}:$PATH"
}

run_mill() {
  local tasks=$1
  local buildLogFile=".heroku/mill-build.log"

  echo "" > $buildLogFile

  for CMD in $tasks; do
    status "Running: mill $CMD"
    JAVA_TOOL_OPTIONS='-Xss8m' mill -i --disable-ticker -D "coursier.cache=.cache" -h .mill ${CMD} | output $buildLogFile

    if [ "${PIPESTATUS[*]}" != "0 0" ]; then
      handle_mill_errors $buildLogFile
    fi
  done
}

cache_copy() {
  rel_dir=$1
  from_dir=$2
  to_dir=$3
  rm -rf $to_dir/$rel_dir
  if [ -d $from_dir/$rel_dir ]; then
    mkdir -p $to_dir/$rel_dir
    cp -pr $from_dir/$rel_dir/. $to_dir/$rel_dir
  fi
}

install_jdk() {
  local install_dir=${1:?}
  local cache_dir=${2:?}

  let start=$(nowms)
  JVM_COMMON_BUILDPACK=${JVM_COMMON_BUILDPACK:-https://buildpack-registry.s3.amazonaws.com/buildpacks/heroku/jvm.tgz}
  mkdir -p /tmp/jvm-common
  curl --retry 3 --silent --location $JVM_COMMON_BUILDPACK | tar xzm -C /tmp/jvm-common --strip-components=1
  source /tmp/jvm-common/bin/util
  source /tmp/jvm-common/bin/java
  source /tmp/jvm-common/opt/jdbc.sh
  mtime "jvm-common.install.time" "${start}"

  let start=$(nowms)
  install_java_with_overlay "${install_dir}" "${cache_dir}"
  mtime "jvm.install.time" "${start}"
}
